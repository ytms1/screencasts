import cv2 # обработка изображений
import pytesseract # готовый алгоритм считывания текста с картинки
from ultralytics import YOLO # импортируем модель для обучения
import easyocr


model = YOLO(r'/Users/vaceslav/Documents/IT/chempionat/screencasts/runs/detect/tune/weights/best.pt') # загружаем лучшие веса после обучения модели

def find_number(image): # создание основной функции API, на вход принимаем путь к фотографии
    image = cv2.imread(image) # объявление переменной с фоткой 
    box = model(image) # проверяем на основе данных модели, есть ли номерная рамка
    if len(box[0].boxes.xyxy) < 0: return 1 # проверка на наличие номера
    x_min, y_min, x_max, y_max = map(int, box[0].boxes.xyxy[0].split()) # достаем координаты рамки сразу в целых числах
    image = image[y_min:y_max, x_min:x_max] # обрезаем картинку по координатам
    image = cv2.resize(image, None, fx=3, fy=3, interpolation=cv2.INTER_CUBIC) # увеличиваем предполагаемую рамку
    image = cv2.GaussianBlur(image, (5, 5), 0) # убираем лишние шумы на фото
    image = cv2.convertScaleAbs(image, alpha=1.5, beta=0) # повышаем яркость и контраст

    reader = easyocr.Reader(['en'])
    # Распознаем номер на обрезанной картинке с помощью ранее созданного экземляра объекта reader
    text = reader.readtext(
            image=image,
            allowlist='ABCEHKMOPTXY0123456789', # Даем список символов которые должна стремится распознать модель исключая все остальные символы
            paragraph=True) # Выводим результаты детекции в удобочитаемом виде

    if len(text) < 3 or len(text) > 12: return 2
    return text, box[0]

def get_number(image):
    out = find_number(image)
    cases = {
        1: 'Номер на картинке не обнаружен',
        2: 'Ошибка при обработке картинки, попробуйте снова'
    }
    try:
        return cases[out]
    except: 
        return f'Предполагаемый номер: {out}'