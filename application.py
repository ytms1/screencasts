import sys # взаимодействия с системой
from PyQt6.QtWidgets import QPushButton, QMainWindow, QApplication, QFileDialog, QLabel, QVBoxLayout, QWidget # импорт необходимых виджетов
from PyQt6.QtGui import QPixmap # импорт для отображения изображения в интерфейсе
from api import get_number # получение результата после работы с номером

class GetPicture(QMainWindow): # создаем класс для получения картинки, содержащий QMainWindow
    def __init__(self): # инициализация элементов, которые будут участвовать в приложении
        super.__init__() # унаследуем элементы от QMainWindow
        self.setWindowTitle('Считывание гос. номера')
        widget = QWidget() # создаем виджет
        layout = QVBoxLayout() # создаем пространство для всех элементов
        widget.setLayout(layout) # устанавливаем к виджету подготовленное пространство
        self.label = QLabel() # объявляем переменную, в которой отображаем номер авто
        self.picture = QLabel() # объявляем переменную, в которой будем отображать фотку
        button = QPushButton('Загрузить фотографию') # объявляем кнопку для загрузки картинки с номером
        button.clicked.connect(self.use_API) # вызываем API

        # добавляем все элементы по порядку
        layout.addWidget(button)
        layout.addWidget(self.label)
        layout.addWidget(self.picture)

        self.setCentralWidget(widget) # ставим виджет в центр
    
    def use_API(self): # создание метода для работы API
        # добавляем виджет для работы с кнопкой
        result = QFileDialog.getOpenFileName(self, 'Выберите файл', '/Users/Server/Desktop/NumbersProject', 'BMP File(*.bmp)')
        pixmap = QPixmap() # объявляем класс QPixMap в переменной
        path = result[0] # берем путь к картинке после действия кнопки
        self.picture.setPixmap(pixmap) # отображаем фото на экране
        if len(path) < 1: return self.label.setText('Фотография не выбрана') # проверка на существование фотографии
        self.label.setText(get_number(path)) # вывод результата с вызовом API


# рендеринг приложения
app = QApplication(sys.argv)
window = GetPicture()
window.show()
app.exec()